import 'dart:math';

void main(List<String> args) {
  //Write a program in a dart to create an age variable and assign a null value to it using ?.
  int? age = null;
  print(age);
  //Write a function named generateRandom() in dart that randomly returns 100 or null.
  // Also, assign a return value of the function to a variable named status that can’t be null.
  // Give status a default value of 0, if generateRandom() function returns null.
  var list = [100, null];
  int? status = list[generateRandom()];
  status ??= 0;
  print(status);
}

int generateRandom() {
  Random random = new Random();
  return random.nextInt(2);
}

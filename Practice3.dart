import 'dart:math';

void main(List<String> args) {
  printName();

  printEvenNumber();

  num ar = area(5);
  print("Điện tích hình tròn: $ar");

  if(isPrimeNumber(4)){
    print("Là số nguyên tố!");
  }else{
    print("Không là số nguyên tố!");
  }

  num py = pytago(2, 3);
  print("Cạnh huyền của tam giác vuông 2^2 + 3^2= $py");

  String text = reverse("Hello word");
  print("Hello word sau khi đảo ngược $text");

  num pow = printPower(5, 3);
  print("Lũy thừa của 5^3=$pow");
}

//Question 1 write a program in Dart to create print your own name using function.
void printName() {
  print("Đỗ Quang Hào");
}

//Question 2 write a program in Dart to print even numbers between intervals using function
void printEvenNumber() {
  for (int i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      print("Số chẵn là $i");
    }
  }
}

//Question 3 write a program in Dart that find the number is prime or not using function.
bool isPrimeNumber(int a) {
  if (a < 2) {
    return false;
  }
  if (a % 2 == 0) {
    return false;
  }
  return true;
}

//Question 4 write a program in Dart that find the area of a circle using function.
num area(int r) {
  return pi * pow(r, 2);
}

//Question 5 write a program in a dart that implements the Pythagorean theorem using function.
num pytago(int a, int b) {
  return pow(a, 2) + pow(b, 2);
}

//Question 6 write a program in Dart to reverse a String using function.
String reverse(String input) {
  return input.split('').reversed.join();
}

//Question 7 write a program in Dart to calculate power of a certain number. For e.g 5^3=125
num printPower(int num, int num1) {
  return pow(num, num1);
}

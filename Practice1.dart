import 'dart:io';
import 'dart:math';

void main() {
  //Question 1 write a program to print your name in Dart.
  String name = "Đỗ Quang Hào";
  print("Tôi tên là: $name");
  //Question 2 declare constant type of int set value 7.
  int num1 = 7;
  print("Số nguyên: $num1");
  //Question 3 write a program in Dart that finds simple interest. Formula= (p * t * r) / 100
  int p = 10;
  int t = 12;
  int r = 15;
  var formula = (p * t * r) / 100;
  print("Kết quả: $formula");
  //Question 4 write a program to print an integer entered by the user.
  print("Nhập số nguyên:");
  int num = int.parse(stdin.readLineSync()!);
  print("Số bạn vừa nhập = ${num}");
  //Question 5 write a program to print String entered by the user.
  print("Nhập tên bạn:");
  String? inputName = stdin.readLineSync();
  print("Tên của bạn: ${inputName}");
  //Question 6 write a program to multiply two floating-point numbers.
  double num2 = 11.5;
  double num3 = 12.3;
  double multiplication = num2 * num3;
  print("Tích hai số = $multiplication");
  //Question 7 write a program to find quotient and remainder of two integers.
  int num4 = 5;
  int num5 = 3;
  double div = num4 / num5;
  int mod = num4 % num5;
  print("Chia lấy dư: $div");
  print("Chia không lấy dư: $mod");
  //Question 8 write a program to swap two numbers.
  print("Trước hoán đổi $num4 vs $num5");
  int hd = num4;
  num4 = num5;
  num5 = hd;
  print("Sau hoán đổi $num4 vs $num5");
  //Question 9 write a program to calculate the square of a number.
  var result = pow(num5, 2);
  print("Phương trình bình phương của $num5 = $result");
  //Question 10 write a program in Dart to remove all whitespaces from String.
  String text = "Xóa khoảng trắng khỏi chuỗi   ";
  print(text.trim());
  //Question 11 write a dart program to convert String to int.
  String strValue = "1";
  int intValue = int.parse(strValue);
  print(intValue);
}

import 'Camera.dart';
import 'Cat.dart';
import 'House.dart';
import 'Laptop.dart';

void main(List<String> args) {
  //Write a dart program to create a class Laptop with properties [id, name, ram]
  // and create 3 objects of it and print all details.
  Laptop laptop1 = new Laptop(1, "Lenovo", "8G");
  Laptop laptop2 = new Laptop(1, "Lenovo", "8G");
  Laptop laptop3 = new Laptop(1, "Lenovo", "8G");
  List<Laptop> listlaptop = [laptop1, laptop2, laptop3];
  for (var laptop in listlaptop) {
    laptop.displayLaptopDetails();
  }
  //Write a dart program to create a class House with properties [id, name, price].
  //Create a constructor of it and create 3 objects of it. Add them to the list and print all details.
  List<House> listHouse = [];
  listHouse.add(new House(1, "House1", 1900.0));
  listHouse.add(new House(2, "House2", 1900.0));
  listHouse.add(new House(3, "House3", 1900.0));
  for (var house in listHouse) {
    house.displayHouseDetails();
  }
  //Write a dart program to create an enum class for gender [male, female, others] and print all values.
  for (var gender in gender.values) {
    print(gender);
  }
  //Write a dart program to create a class Animal with properties [id, name, color].
  //Create another class called Cat and extends it from Animal.
  //Add new properties sound in String. Create an object of a Cat and print all details.
  Cat cat = new Cat(1, "Hoa", "while", "Meo, Meo, Meo");
  print("id: " +
      cat.id.toString() +
      " ,Name: " +
      cat.name.toString() +
      " ,Colos: " +
      cat.color.toString() +
      " ,Sound: " +
      cat.sound.toString());
  //Write a dart program to create a class Camera with private properties [id, brand, color, price].
  // Create a getter and setter to get and set values. Also, create 3 objects of it and print all details.
  Camera camera = new Camera();
  camera.setId = 1;
  camera.setName = "Camera Gopro Hero 10 1";
  camera.setPrice = 9000;
  camera.colos = "Màu đen";
  Camera camera1 = new Camera();
  camera1.setId = 2;
  camera1.setName = "Camera Gopro Hero 10 2";
  camera1.setPrice = 9000;
  camera1.colos = "Màu đen";
  Camera camera2 = new Camera();
  camera2.setId = 3;
  camera2.setName = "Camera Gopro Hero 10 3";
  camera2.setPrice = 9000;
  camera2.colos = "Màu đen";
  List<Camera> listCamera = [camera, camera1, camera2];
  for (var camera in listCamera) {
    print("Id: " +
        camera.getId.toString() +
        " ,Name: " +
        camera.getName.toString() +
        " ,Colos: " +
        camera.getColos.toString() +
        " ,Price: " +
        camera.getPrice.toString());
  }
  ;
}

enum gender { male, female, others }

class House {
  int? id;
  String? name;
  double? price;
  House(int? id, String? name, double? price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }
  void displayHouseDetails() {
    print("Id: " + id.toString()+", Name: " + name.toString()+", Ram: " + price.toString());
  }
}

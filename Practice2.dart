void main(List<String> args) {
  //Question 1 write a dart program to check if the number is odd or even.
  int num = 6;
  if (num % 2 == 0) {
    print("Số chẵn");
  } else {
    print("Số lẻ");
  }
  //Question 2 write a dart program to check whether a character is a vowel or consonant.
  switch ("h") {
    case "u":
      print("Đây là nguyên âm!");
      break;
    case "e":
      print("Đây là nguyên âm!");
      break;
    case "o":
      print("Đây là nguyên âm!");
      break;
    case "a":
      print("Đây là nguyên âm!");
      break;
    case "i":
      print("Đây là nguyên âm!");
      break;
    default:
      print("Đây là phụ âm!");
      break;
  }
  //Question 3 write a dart program to check whether a number is positive, negative, or zero.
  int num1 = 2;
  if (num1 > 0) {
    print("Đây là số dương!");
  } else {
    if (num1 == 0) {
      print("Đây là số 0!");
    } else {
      print("Đây là số âm!");
    }
  }

  //Question 4 write a dart program to print your name 100 times.
  for (int i = 1; i <= 100; i++) {
    print("Đỗ Quang Hào thứ $i");
  }
  //Question 5 write a dart program to calculate the sum of natural numbers.
  int total = 0;
  for (int i = 0; i <= 10; i++) {
    total = total + i;
  }
  print("Tổng các số tự nhiên = $total");
  //Question 6 write a dart program to generate multiplication tables of 5.
  for (int i = 0; i <= 10; i++) {
    int toal = i * 5;
    print("$i * 5 = $toal");
  }
  //Question 7 write a dart program to generate multiplication tables of 1-9.
  for (int i = 1; i <= 9; i++) {
    print("Bản cửu chương $i");
    for (int j = 1; j <= 10; j++) {
      int toal = j * i;
      print("$j * $i = $toal");
    }
  }
  //Question 8 write a dart program to create a simple calculator using a switch case.
  double num2 = 10;
  double num3 = 20;
  double toal = 0;
  switch ("+") {
    case "+":
      toal = num2 + num3;
      print("Kết quả: $toal");
      break;
    case "-":
      toal = num2 - num3;
      print("Kết quả: $toal");
      break;
    case "*":
      toal = num2 * num3;
      print("Kết quả: $toal");
      break;
    case "/":
      toal = num2 / num3;
      print("Kết quả: $toal");
      break;
    default:
      print("Vui lòng nhập lại!");
      break;
  }
  //Question 9 write a dart program to print 1 to 100 but not 41.
  for (int i = 1; i <= 100; i++) {
    if (i == 41) {
      continue;
    }
    print("Sô $i");
  }
}

class Laptop {
  int? id;
  String? name;
  String? ram;

  Laptop(int? id, String? name, String? ram) {
    this.id = id;
    this.name = name;
    this.ram = ram;
  }
  
  void displayLaptopDetails() {
    print("Id: " + id.toString()+" ,Name: " + name.toString()+" ,Ram: " + ram.toString());
  }
  
}

void main(List<String> args) {
  //Create an empty list of type string called days. Use the add method to add names of 7 days and print all days.
  List<String> days=[];
  days.add("Monday");
  days.add("Tuesday");
  days.add("Wednesday");
  days.add("Thursday");
  days.add("Friday");
  days.add("Saturday");
  days.add("Sunday");
  print(days);
  //Create a map with name, address, age, country keys and store values to it.
  // Update country name to other country and print all keys and values.
  Map<String,dynamic>information={'name':'Đỗ Quang Hao','address':'Hưng yên','age':21,'Country':'Việt Nam'};
  information['Country']='America';
  print(information);
}